import React, { useState } from "react";
import "./App.css";

const sentences = [
  { text: "Kva likar han å _?", alternatives: ["ete", "et", "åt"] },
  { text: "Han _ kva som helst.", alternatives: ["et", "ete", "åt"] },
  { text: "Kva _ Per på med nå?", alternatives: ["driv", "drive", "dreiv"] },
  //{text: "Per _ ein tur i skogen nå.", alternatives: ["gjeng", ""]},
  // {text: "", alternatives: [""]},
];

interface Task {
  text: string;
  alternatives: string[];
}

export default function App() {
  const [count, setCount] = useState(0);
  const [tasks, setTasks] = useState<Task[]>();

  async function readFile(event: React.ChangeEvent<HTMLInputElement>) {
    const fileList = event.target.files;
    if (!fileList || !fileList.length) return;
    const file = fileList[0];

    if (file.type && file.type !== "text/plain") {
      alert("Ugyldig filtype");
      return;
    }

    /*const reader = new FileReader();
    reader.addEventListener("load", (event) => {
      if (!event.target) return;
      const text = event.target.result;
      console.log(text);
    });
    reader.readAsDataURL(file);*/

    const text = await file.text();
    const newTasks = text
      .trim()
      .split("\n")
      .map((line) => {
        const parts = line.trim().split("|");
        const [text, ...alternatives] = parts;
        console.log(alternatives);
        return { text, alternatives };
      });
    setTasks(newTasks);
  }

  if (!tasks) {
    return (
      <div>
        <label>
          Last upp oppgåvefil: <input type="file" onChange={readFile} />
        </label>
        eller{" "}
        <a href="#" onClick={() => setTasks(sentences)}>
          bruk demodata
        </a>
        .
      </div>
    );
  }

  const task = tasks[count];
  const alternatives = [...task.alternatives];

  function answer(event: React.MouseEvent<HTMLButtonElement>) {
    const answeredAlt = event.currentTarget.innerText;
    if (task.alternatives[0] === answeredAlt) {
      alert("Riktig");
      if (tasks && count === tasks.length - 1) {
        setCount(0);
        setTasks(undefined);
      } else setCount(count + 1);
    } else alert("Feil");
  }

  return (
    <div className="app">
      <p>{task.text}</p>
      <div className="answers">
        {alternatives
          .sort((a, b) => 0.5 - Math.random())
          .map((alt) => (
            <button key={alt} onClick={answer}>
              {alt}
            </button>
          ))}
      </div>
    </div>
  );
}
